
/*
   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../MdtSimHitToxAODCnvAlg.h"
#include "../xAODSimHitToMdtCnvAlg.h"
#include "../xAODSimHitToMdtMeasCnvAlg.h"


DECLARE_COMPONENT(MdtSimHitToxAODCnvAlg)
DECLARE_COMPONENT(xAODSimHitToMdtCnvAlg)
DECLARE_COMPONENT(xAODSimHitToMdtMeasCnvAlg)