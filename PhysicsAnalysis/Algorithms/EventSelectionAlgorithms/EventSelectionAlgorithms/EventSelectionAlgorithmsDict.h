/*
   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Baptiste Ravina

#ifndef EVENT_SELECTION_ALGORITHMS_DICT_H
#define EVENT_SELECTION_ALGORITHMS_DICT_H

#include <EventSelectionAlgorithms/ChargeSelectorAlg.h>
#include <EventSelectionAlgorithms/MissingETPlusTransverseMassSelectorAlg.h>
#include <EventSelectionAlgorithms/MissingETSelectorAlg.h>
#include <EventSelectionAlgorithms/DileptonInvariantMassSelectorAlg.h>
#include <EventSelectionAlgorithms/DileptonInvariantMassWindowSelectorAlg.h>
#include <EventSelectionAlgorithms/TransverseMassSelectorAlg.h>
#include <EventSelectionAlgorithms/SaveFilterAlg.h>
#include <EventSelectionAlgorithms/SignEnums.h>
#include <EventSelectionAlgorithms/NObjectPtSelectorAlg.h>

#endif
