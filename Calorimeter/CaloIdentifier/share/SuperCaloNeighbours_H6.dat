# File and Version Information:
# $Id: SuperCaloNeighbours_H6.dat,v 1.1 2004-09-25 12:18:12 menke Exp $
#
# Author: Sven Menke <menke@mppmu.mpg.de>
# Date:   Sat Sep 25 13:48:50 2004
#
# ranges of expanded calorimeter identifiers are mapped to create lookup
# tables for the super calo neighbours. The mapping in eta has to be given
# explicitely, while side symmetry and the mapping in phi can be automated.
# The automation in phi works only on contigous ranges. This file is for
# the combined testbeam 2004 in H6
#
# Syntax:
# for each mapped region: Header and Entry lines until an empty line
# denotes the end of the block. The Header consists of:
# <Type> <Name> <SourceRange> <TargetRange> [<SideDirective>] [<PhiDirective>] 
# <Type>: nextSuperCalo or prevSuperCalo denotes the direction of the map
# <Name>: any string without blanks, white-space etc.
# <Source/TargetRange>: A cell-level ExpandedIdentifier Range
# <SideDirective>: calcSide(i1,i2) maps sign of the field i1 in SourceRange to
#                                  the sign of field i2 in TargetRange.
# <PhiDirective>: calcPhi(i1,i2) maps the field i1 in SourceRange to
#                                the field i2 in TargetRange by scaling.
#
nextSuperCalo EME2IW/HEC01 4/1/3/2/0/0:6/20:27 4/2/2/0/1/0:3/10:13 calcPhi(6,6)
4/1/3/2/0/0/20 4/2/2/0/1/0/10
4/1/3/2/0/1/20 4/2/2/0/1/0/10
4/1/3/2/0/2/20 4/2/2/0/1/1/10
4/1/3/2/0/3/20 4/2/2/0/1/1/10
4/1/3/2/0/4/20 4/2/2/0/1/2/10
4/1/3/2/0/5/20 4/2/2/0/1/2/10
4/1/3/2/0/6/20 4/2/2/0/1/3/10

prevSuperCalo HEC01/EME2IW 4/2/2/0/1/0:3/10:13 4/1/3/2/0/0:6/20:27 calcPhi(6,6)
4/2/2/0/1/0/10 4/1/3/2/0/0/20 4/1/3/2/0/0/21 4/1/3/2/0/1/20 4/1/3/2/0/1/21 
4/2/2/0/1/1/10 4/1/3/2/0/2/20 4/1/3/2/0/2/21 4/1/3/2/0/3/20 4/1/3/2/0/3/21 
4/2/2/0/1/2/10 4/1/3/2/0/4/20 4/1/3/2/0/4/21 4/1/3/2/0/5/20 4/1/3/2/0/5/21 
4/2/2/0/1/3/10 4/1/3/2/0/6/20 4/1/3/2/0/6/21

nextSuperCalo HEC01/FCAL1 4/2/2/0/1/2:3/9:14 4/3/2/1/0:22/4:7
4/2/2/0/1/2/9 4/3/2/1/0/4
4/2/2/0/1/2/10 4/3/2/1/0/5 4/3/2/1/1/5
4/2/2/0/1/2/11 4/3/2/1/7/5 4/3/2/1/8/5
4/2/2/0/1/2/12 4/3/2/1/0/6
4/2/2/0/1/2/13 4/3/2/1/6/6
4/2/2/0/1/2/14 4/3/2/1/0/7
4/2/2/0/1/3/9 4/3/2/1/1/4 4/3/2/1/2/4 4/3/2/1/3/4 4/3/2/1/4/4 4/3/2/1/5/4 4/3/2/1/6/4 4/3/2/1/7/4 4/3/2/1/8/4 4/3/2/1/9/4 4/3/2/1/10/4 4/3/2/1/11/4
4/2/2/0/1/3/10 4/3/2/1/2/5 4/3/2/1/3/5 4/3/2/1/9/5 4/3/2/1/10/5 4/3/2/1/13/5 4/3/2/1/15/5 4/3/2/1/18/5 4/3/2/1/19/5 4/3/2/1/22/5
4/2/2/0/1/3/11 4/3/2/1/4/5 4/3/2/1/5/5 4/3/2/1/6/5 4/3/2/1/11/5 4/3/2/1/12/5 4/3/2/1/14/5 4/3/2/1/16/5 4/3/2/1/17/5 4/3/2/1/20/5 4/3/2/1/21/5
4/2/2/0/1/3/12 4/3/2/1/1/6 4/3/2/1/2/6 4/3/2/1/9/6 4/3/2/1/10/6 4/3/2/1/12/6 4/3/2/1/15/6 4/3/2/1/17/6 4/3/2/1/18/6 4/3/2/1/20/6
4/2/2/0/1/3/13 4/3/2/1/3/6 4/3/2/1/4/6 4/3/2/1/5/6 4/3/2/1/7/6 4/3/2/1/8/6 4/3/2/1/11/6 4/3/2/1/13/6 4/3/2/1/14/6 4/3/2/1/16/6 4/3/2/1/19/6 4/3/2/1/22/6
4/2/2/0/1/3/14 4/3/2/1/1/7 4/3/2/1/2/7 4/3/2/1/3/7 4/3/2/1/4/7 4/3/2/1/5/7 4/3/2/1/6/7 4/3/2/1/7/7 4/3/2/1/8/7 4/3/2/1/9/7 4/3/2/1/10/7 4/3/2/1/21/6

prevSuperCalo FCAL1/HEC01 4/3/2/1/0:22/4:7 4/2/2/0/1/2:3/9:14
4/3/2/1/0/4 4/2/2/0/1/2/9 
4/3/2/1/0/5 4/2/2/0/1/2/10 
4/3/2/1/0/6 4/2/2/0/1/2/12 
4/3/2/1/0/7 4/2/2/0/1/2/14 
4/3/2/1/1/4 4/2/2/0/1/3/9 
4/3/2/1/1/5 4/2/2/0/1/2/10 
4/3/2/1/1/6 4/2/2/0/1/3/12 
4/3/2/1/1/7 4/2/2/0/1/3/14 
4/3/2/1/10/4 4/2/2/0/1/3/9 
4/3/2/1/10/5 4/2/2/0/1/3/10 
4/3/2/1/10/6 4/2/2/0/1/3/12 
4/3/2/1/10/7 4/2/2/0/1/3/14 
4/3/2/1/11/4 4/2/2/0/1/3/9 
4/3/2/1/11/5 4/2/2/0/1/3/11 
4/3/2/1/11/6 4/2/2/0/1/3/13 
4/3/2/1/12/5 4/2/2/0/1/3/11 
4/3/2/1/12/6 4/2/2/0/1/3/12 
4/3/2/1/13/5 4/2/2/0/1/3/10 
4/3/2/1/13/6 4/2/2/0/1/3/13 
4/3/2/1/14/5 4/2/2/0/1/3/11 
4/3/2/1/14/6 4/2/2/0/1/3/13 
4/3/2/1/15/5 4/2/2/0/1/3/10 
4/3/2/1/15/6 4/2/2/0/1/3/12 
4/3/2/1/16/5 4/2/2/0/1/3/11 
4/3/2/1/16/6 4/2/2/0/1/3/13 
4/3/2/1/17/5 4/2/2/0/1/3/11 
4/3/2/1/17/6 4/2/2/0/1/3/12 
4/3/2/1/18/5 4/2/2/0/1/3/10 
4/3/2/1/18/6 4/2/2/0/1/3/12 
4/3/2/1/19/5 4/2/2/0/1/3/10 
4/3/2/1/19/6 4/2/2/0/1/3/13 
4/3/2/1/2/4 4/2/2/0/1/3/9 
4/3/2/1/2/5 4/2/2/0/1/3/10 
4/3/2/1/2/6 4/2/2/0/1/3/12 
4/3/2/1/2/7 4/2/2/0/1/3/14 
4/3/2/1/20/5 4/2/2/0/1/3/11 
4/3/2/1/20/6 4/2/2/0/1/3/12 
4/3/2/1/21/5 4/2/2/0/1/3/11 
4/3/2/1/21/6 4/2/2/0/1/3/14 
4/3/2/1/22/5 4/2/2/0/1/3/10 
4/3/2/1/22/6 4/2/2/0/1/3/13 
4/3/2/1/3/4 4/2/2/0/1/3/9 
4/3/2/1/3/5 4/2/2/0/1/3/10 
4/3/2/1/3/6 4/2/2/0/1/3/13 
4/3/2/1/3/7 4/2/2/0/1/3/14 
4/3/2/1/4/4 4/2/2/0/1/3/9 
4/3/2/1/4/5 4/2/2/0/1/3/11 
4/3/2/1/4/6 4/2/2/0/1/3/13 
4/3/2/1/4/7 4/2/2/0/1/3/14 
4/3/2/1/5/4 4/2/2/0/1/3/9 
4/3/2/1/5/5 4/2/2/0/1/3/11 
4/3/2/1/5/6 4/2/2/0/1/3/13 
4/3/2/1/5/7 4/2/2/0/1/3/14 
4/3/2/1/6/4 4/2/2/0/1/3/9 
4/3/2/1/6/5 4/2/2/0/1/3/11 
4/3/2/1/6/6 4/2/2/0/1/2/13 
4/3/2/1/6/7 4/2/2/0/1/3/14 
4/3/2/1/7/4 4/2/2/0/1/3/9 
4/3/2/1/7/5 4/2/2/0/1/2/11 
4/3/2/1/7/6 4/2/2/0/1/3/13 
4/3/2/1/7/7 4/2/2/0/1/3/14 
4/3/2/1/8/4 4/2/2/0/1/3/9 
4/3/2/1/8/5 4/2/2/0/1/2/11 
4/3/2/1/8/6 4/2/2/0/1/3/13 
4/3/2/1/8/7 4/2/2/0/1/3/14 
4/3/2/1/9/4 4/2/2/0/1/3/9 
4/3/2/1/9/5 4/2/2/0/1/3/10 
4/3/2/1/9/6 4/2/2/0/1/3/12 
4/3/2/1/9/7 4/2/2/0/1/3/14 

nextSuperCalo FCAL1/HEC21 4/3/2/1/0,1,6,7,8/4:7 4/2/2/2/1/2/9:14 
4/3/2/1/0/4 4/2/2/2/1/2/9 
4/3/2/1/0/5 4/2/2/2/1/2/10 
4/3/2/1/0/6 4/2/2/2/1/2/12 
4/3/2/1/0/7 4/2/2/2/1/2/14 
4/3/2/1/1/5 4/2/2/2/1/2/10 
4/3/2/1/6/6 4/2/2/2/1/2/13 
4/3/2/1/7/5 4/2/2/2/1/2/11 
4/3/2/1/8/5 4/2/2/2/1/2/11 

prevSuperCalo HEC21/FCAL1 4/2/2/2/1/2/9:14 4/3/2/1/0,1,6,7,8/4:7
4/2/2/2/1/2/9 4/3/2/1/0/4
4/2/2/2/1/2/10 4/3/2/1/0/5 4/3/2/1/1/5
4/2/2/2/1/2/11 4/3/2/1/7/5 4/3/2/1/8/5
4/2/2/2/1/2/12 4/3/2/1/0/6
4/2/2/2/1/2/13 4/3/2/1/6/6
4/2/2/2/1/2/14 4/3/2/1/0/7

